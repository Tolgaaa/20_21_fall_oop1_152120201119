#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int n;
    float a, b, c, d;

    cin >> n;
    cin >> a >> b >> c >> d;

    cout << "----------------" << endl;

    cout << "Sum is " << a + b + c + d << endl;
    cout << "Product is " << a * b * c * d << endl;
    cout << "Average is " << (a + b + c + d) / 4 << endl;

    float min;
    if (a < b && a < c && a < d)
    {
        min = a;

    }
    else if (b < a && b < c && b < d)
    {
        min = b;
    }
    else if (c < a && c < b && c < d)
    {
        min = c;
    }
    else
        min = d;

    cout << "Smallest is " << min;


    return min;


}